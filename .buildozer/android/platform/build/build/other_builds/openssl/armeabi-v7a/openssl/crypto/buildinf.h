#ifndef MK1MF_BUILD
    /* auto-generated by util/mkbuildinf.pl for crypto/cversion.c */
    #define CFLAGS cflags
    /*
     * Generate CFLAGS as an array of individual characters. This is a
     * workaround for the situation where CFLAGS gets too long for a C90 string
     * literal
     */
    static const char cflags[] = {
        'c','o','m','p','i','l','e','r',':',' ','/','u','s','r','/','b','i','n',
        '/','c','c','a','c','h','e',' ','a','r','m','-','l','i','n','u','x','-',
        'a','n','d','r','o','i','d','e','a','b','i','-','g','c','c',' ','-','D',
        'A','N','D','R','O','I','D',' ','-','m','a','n','d','r','o','i','d',' ',
        '-','f','o','m','i','t','-','f','r','a','m','e','-','p','o','i','n','t',
        'e','r',' ','-','D','_','_','A','N','D','R','O','I','D','_','A','P','I',
        '_','_','=','1','9',' ','-','I','/','h','o','m','e','/','m','a','x','/',
        'c','r','y','s','t','a','x','-','n','d','k','-','1','0','.','3','.','2',
        '/','p','l','a','t','f','o','r','m','s','/','a','n','d','r','o','i','d',
        '-','1','9','/','a','r','c','h','-','a','r','m',' ','-','i','s','y','s',
        'r','o','o','t',' ','/','h','o','m','e','/','m','a','x','/','c','r','y',
        's','t','a','x','-','n','d','k','-','1','0','.','3','.','2','/','p','l',
        'a','t','f','o','r','m','s','/','a','n','d','r','o','i','d','-','1','9',
        '/','a','r','c','h','-','a','r','m',' ','-','I','/','h','o','m','e','/',
        'm','a','x','/','B','u','r','e','a','u','/','B','X','L','2','/','.','b',
        'u','i','l','d','o','z','e','r','/','a','n','d','r','o','i','d','/','p',
        'l','a','t','f','o','r','m','/','b','u','i','l','d','/','b','u','i','l',
        'd','/','p','y','t','h','o','n','-','i','n','s','t','a','l','l','s','/',
        'm','y','a','p','p','4','/','i','n','c','l','u','d','e','/','p','y','t',
        'h','o','n','3','.','5',' ',' ','-','-','s','y','s','r','o','o','t',' ',
        '/','h','o','m','e','/','m','a','x','/','c','r','y','s','t','a','x','-',
        'n','d','k','-','1','0','.','3','.','2','/','p','l','a','t','f','o','r',
        'm','s','/','a','n','d','r','o','i','d','-','1','9','/','a','r','c','h',
        '-','a','r','m',' ','-','l','m',' ','-','L','/','h','o','m','e','/','m',
        'a','x','/','B','u','r','e','a','u','/','B','X','L','2','/','.','b','u',
        'i','l','d','o','z','e','r','/','a','n','d','r','o','i','d','/','p','l',
        'a','t','f','o','r','m','/','b','u','i','l','d','/','b','u','i','l','d',
        '/','l','i','b','s','_','c','o','l','l','e','c','t','i','o','n','s','/',
        'm','y','a','p','p','4','/','a','r','m','e','a','b','i','-','v','7','a',
        ' ','-','L','/','h','o','m','e','/','m','a','x','/','c','r','y','s','t',
        'a','x','-','n','d','k','-','1','0','.','3','.','2','/','s','o','u','r',
        'c','e','s','/','c','r','y','s','t','a','x','/','l','i','b','s','/','a',
        'r','m','e','a','b','i','-','v','7','a',' ','-','l','c','r','y','s','t',
        'a','x',' ','-','I','.',' ','-','I','.','.',' ','-','I','.','.','/','i',
        'n','c','l','u','d','e',' ',' ','-','f','P','I','C',' ','-','D','O','P',
        'E','N','S','S','L','_','P','I','C',' ','-','D','O','P','E','N','S','S',
        'L','_','T','H','R','E','A','D','S',' ','-','D','_','R','E','E','N','T',
        'R','A','N','T',' ','-','m','a','r','c','h','=','a','r','m','v','7','-',
        'a',' ','-','m','a','n','d','r','o','i','d',' ','-','I','/','i','n','c',
        'l','u','d','e',' ','-','B','/','l','i','b',' ','-','O','3',' ','-','f',
        'o','m','i','t','-','f','r','a','m','e','-','p','o','i','n','t','e','r',
        ' ','-','W','a','l','l',' ','-','D','O','P','E','N','S','S','L','_','B',
        'N','_','A','S','M','_','M','O','N','T',' ','-','D','O','P','E','N','S',
        'S','L','_','B','N','_','A','S','M','_','G','F','2','m',' ','-','D','S',
        'H','A','1','_','A','S','M',' ','-','D','S','H','A','2','5','6','_','A',
        'S','M',' ','-','D','S','H','A','5','1','2','_','A','S','M',' ','-','D',
        'A','E','S','_','A','S','M',' ','-','D','B','S','A','E','S','_','A','S',
        'M',' ','-','D','G','H','A','S','H','_','A','S','M','\0'
    };
    #define PLATFORM "platform: android-armv7"
    #define DATE "built on: Mon Apr  9 18:14:37 2018"
#endif
