**This project is the implementation of BXL² an application used by the company "La Fabrique Des Voisins" in order to create a colaborative platform between people of the same 
neighborhood.**


![](textures/about1.jpg)
![](textures/about2.jpg)



This Android App is implemented using Kivy, a python framework to build cross platform applications.
[Kivy](https://kivy.org/#home)



This project was part of my bachelor thesis in 2018.


*Powered by BXL2. All rights reserved.*

[website](http://lafabriquedesvoisins.be/)

Note : I don't think the company is still working